from PyQt5.QtCore import Qt, QByteArray, QBuffer, QIODevice
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QWidget, QLabel, QSlider, QPushButton, QGridLayout, QFileDialog, QMessageBox
from PIL import Image
from PIL.ImageQt import ImageQt
from enum import Enum
from io import BytesIO
from krita import DockWidget, DockWidgetFactory, DockWidgetFactoryBase
from .generator import Generator

DOCKER_NAME = 'Tileset Generator'
DOCKER_ID = 'pyKrita_tilesetgenerator'

class TileType(Enum):
    base = 0
    border = 1


class MarkerType(Enum):
    top = 0
    bottom = 1


class TilesetGenerator(DockWidget):


    def __init__(self):
        super().__init__()
        self.setWindowTitle(DOCKER_NAME)

        self.tileSize = None
        self.baseTile = None
        self.displayBaseTile = None
        self.borderTile = None
        self.displayBorderTile = None

        self.markerType = MarkerType.bottom
        self.bottomMarkerHeight = 80
        self.topMarkerHeight = 100

        mainWidget = QWidget(self)
        self.setWidget(mainWidget)

        # Components
        labelStyle = "QLabel { background-color : #424242; }"

        self.baseTileImage = QLabel(mainWidget)
        self.baseTileImage.setFixedSize(100, 100)
        self.baseTileImage.setStyleSheet(labelStyle)

        self.borderTileImage = QLabel(mainWidget)
        self.borderTileImage.setFixedSize(100, 100)
        self.borderTileImage.setStyleSheet(labelStyle)

        self.borderMarker = QSlider(Qt.Vertical)
        self.borderMarker.setFixedHeight(100)
        self.borderMarker.setFixedWidth(20)
        self.borderMarker.setMinimum(0)
        self.borderMarker.setMaximum(100)
        self.borderMarker.setValue(self.bottomMarkerHeight)

        self.setMarkerButton = QPushButton("B", mainWidget)
        self.setMarkerButton.setFixedWidth(20)
        self.setMarkerButton.setCheckable(True)
        self.setMarkerButton.toggle()

        self.setMarker()

        setBaseTileButton = QPushButton("Load base tile", mainWidget)
        setBorderTileButton = QPushButton("Load border tile", mainWidget)
        generateButton = QPushButton("Generate", mainWidget)

        # Actions
        self.setMarkerButton.clicked.connect(self.setMarkerType)
        self.borderMarker.valueChanged.connect(self.setMarker)
        setBaseTileButton.clicked.connect(lambda: self.selectImage(TileType.base))
        setBorderTileButton.clicked.connect(lambda: self.selectImage(TileType.border))
        generateButton.clicked.connect(self.generate)

        # Layout
        mainWidget.setLayout(QGridLayout())
        mainWidget.layout().addWidget(self.setMarkerButton, 0, 2, 1, 1)
        mainWidget.layout().addWidget(self.baseTileImage, 1, 0, 1, 2)
        mainWidget.layout().addWidget(self.borderMarker, 1, 2, 1, 1)
        mainWidget.layout().addWidget(self.borderTileImage, 1, 3, 1, 1)
        mainWidget.layout().addWidget(setBaseTileButton, 2, 0, 1, 2)
        mainWidget.layout().addWidget(setBorderTileButton, 2, 2, 1, 2)
        mainWidget.layout().addWidget(generateButton, 3, 0, 1, 4)


    def setMarkerType(self):
        isChecked = self.setMarkerButton.isChecked()
        self.markerType = MarkerType.bottom if isChecked else MarkerType.top

        value = self.bottomMarkerHeight if isChecked else self.topMarkerHeight
        self.borderMarker.setValue(value)

        label = "B" if isChecked else "T"
        self.setMarkerButton.setText(label)
        

    def setMarker(self):
        height = 100
        selectedHeight = self.borderMarker.value()
        size = (height, height)

        if self.markerType is MarkerType.bottom: 
            self.bottomMarkerHeight = selectedHeight
        if self.markerType is MarkerType.top: 
            self.topMarkerHeight = selectedHeight

        if self.topMarkerHeight < self.bottomMarkerHeight: 
            self.topMarkerHeight = self.bottomMarkerHeight + 1
            self.borderMarker.setValue(self.topMarkerHeight)

        markerImage=Image.new("RGBA", size)
        markedImage = self.displayBorderTile.copy() if self.displayBorderTile else markerImage

        for markerType in [MarkerType.bottom, MarkerType.top]:
            currentHeight = self.bottomMarkerHeight if markerType == MarkerType.bottom else self.topMarkerHeight
            currentHeight = 100 - currentHeight
            color = (118, 151, 180) if markerType is MarkerType.bottom else (255, 0, 0)
            pixels = list(markerImage.getdata())
            pixels = [
                color if height * currentHeight <= i < height * currentHeight + height else x 
                for i, x in enumerate(pixels)
            ]
            markerImage.putdata(pixels)
            markedImage.paste(markerImage, (0,0), markerImage)

        pixmap = QPixmap.fromImage(ImageQt(markedImage))
        self.borderTileImage.setPixmap(pixmap)


    def selectImage(self, tileType):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ =QFileDialog.getOpenFileName(self, "file", "","All Files (*);;", options=options)
        if not fileName: return
        with Image.open(fileName) as tile:
            width, height = tile.size
            if width != height: 
                self.showMessage("The image width {0} is not equal to its height {1}".format(width, height))
                return
            if self.tileSize is not None and self.tileSize != width:
                self.showMessage("the image dimensions {0}x{1} do not match that of the other image {2}x{2}".format(width, height, self.tileSize))
                return
            self.tileSize = width

            displayedImage = tile.resize(size=(100, 100), resample=Image.NEAREST, box=None)
            pixmap = QPixmap.fromImage(ImageQt(displayedImage))

            if tileType is TileType.base: 
                self.baseTile = tile
                self.displayBaseTile = displayedImage
                self.baseTileImage.setPixmap(pixmap)
            if tileType is TileType.border:
                self.borderTile = tile
                self.displayBorderTile = displayedImage
                self.borderTileImage.setPixmap(pixmap)
                self.setMarker()


    def showMessage(self, message):
        QMessageBox.information(QWidget(), "Error", message)


    def generate(self):
        if not self.baseTile:
            self.showMessage("No base tile found")
            return
        
        self.borderTile = self.borderTile if self.borderTile else Image.new('RGBA', (self.tileSize, self.tileSize))
        thinBorderRange = (100 - self.topMarkerHeight, 100 - self.bottomMarkerHeight)
        generator = Generator(self.baseTile, self.borderTile, thinBorderRange)
        baseImage = generator.generateBaseTileset()
        borderImage = generator.generateBorderTileset()

        basePixels = QByteArray(self.getPixels(baseImage).getvalue())
        borderPixels = QByteArray(self.getPixels(borderImage).getvalue())

        newDocument = Krita.instance().createDocument(self.tileSize * 11, self.tileSize * 5, "new tileset", "RGBA", "U8", "", 120.0)
        Krita.instance().activeWindow().addView(newDocument)

        root = newDocument.rootNode()
        baseLayer = newDocument.createNode("base", "paintlayer")
        borderLayer = newDocument.createNode("border", "paintlayer")
        root.addChildNode(baseLayer, None)
        newDocument.activeNode().setPixelData(basePixels, 0, 0, *baseImage.size)
        newDocument.activeNode().setVisible(True)
        root.addChildNode(borderLayer, None)
        newDocument.activeNode().setPixelData(borderPixels, 0, 0, *borderImage.size)


    def getPixels(self, image):
        image = image.convert("RGBA")
        pixels = list(image.getdata())
        pixels = [value for pixel in pixels for value in (pixel[2], pixel[1], pixel[0], pixel[3])]
        return BytesIO(bytearray(pixels))


    def canvasChanged(self, canvas):
        pass


instance = Krita.instance()
dock_widget_factory = DockWidgetFactory(DOCKER_ID, 
    DockWidgetFactoryBase.DockRight, 
    TilesetGenerator)

instance.addDockWidgetFactory(dock_widget_factory)
