#!/usr/bin/python

from PIL import Image
from io import BytesIO
from enum import Enum

import sys
from pathlib import Path

class Orientation(Enum):
    left = 0
    right = 1
    top = 2
    bottom = 3

class CornerPosition(Enum):
    topLeft = 0
    topRight = 1
    bottomLeft = 2
    bottomRight = 3

class CornerType(Enum):
    none = 0
    single = 1
    double = 2

class BlobMap():
    skippedBaseTiles = [(0,4), (1,4), (2,4), (3,4), 
                (9,4), (10,0), (10,1), (10,4)]

    topLeftCorner = [(0,0), (0,3), (3,3), (5,2)]
    topRightCorner = [(2,0), (3,3), (6,2)]
    bottomLeftCorner = [(0,2), (0,3), (3,2), (3,3), (5,1)]
    bottomRightCorner = [(2,2), (2,3), (3,2), (3,3), (6,1)]
    corners = topLeftCorner + topRightCorner + bottomLeftCorner + bottomRightCorner

    leftBorder = [(0, 0), (0, 1), (0, 2), (0, 3), 
                (3,0), (3,1), (3,2), (3,3)]
    rightBorder = [(2,0), (2,1), (2,2), (3,0), 
                (3,1), (2,3), (3,2), (3,3)]
    topBorder = [(0,0), (1,0), (2,0), (3,0), 
                (3,3), (0,3), (1,3), (2,3), (5,2), (6,2)]
    bottomBorder = [(0,2), (1,2), (2,2), (1,3), 
                (2,3), (3,2), (3,3), (0,3), (5,1), (6,1)]

    @classmethod
    def getOrientations(cls, position):
        orientations = []
        if position in cls.leftBorder: orientations.append(Orientation.left)
        if position in cls.rightBorder: orientations.append(Orientation.right)
        if position in cls.topBorder: orientations.append(Orientation.top)
        if position in cls.bottomBorder: orientations.append(Orientation.bottom)
        return orientations

    @classmethod
    def getCornerPositions(cls, position):
        cornerPositions = []
        if position in cls.topLeftCorner: cornerPositions.append(CornerPosition.topLeft)
        if position in cls.topRightCorner: cornerPositions.append(CornerPosition.topRight)
        if position in cls.bottomLeftCorner: cornerPositions.append(CornerPosition.bottomLeft)
        if position in cls.bottomRightCorner: cornerPositions.append(CornerPosition.bottomRight)
        return cornerPositions

    @classmethod
    def getCornerType(cls, position, orientation):
        cornerCount = cls.corners.count(position)
        cornerType = CornerType.none
        if cornerCount > 0 : cornerType = CornerType.single
        if cornerCount > 1:
            isTopBottom = orientation in [Orientation.bottom, Orientation.top]
            isLeftRight = orientation in [Orientation.left, Orientation.right]
            if (position in cls.leftBorder and position in cls.rightBorder and isTopBottom
               or position in cls.bottomBorder and position in cls.topBorder and isLeftRight):
                cornerType = CornerType.double
        
        return cornerType


class Generator:

    def __init__(self, baseTile, borderTile, thinBorderRange):
        self.baseTile = baseTile
        self.borderTile = borderTile
        self.thinBorderRange = thinBorderRange
        self.hasThinBorder = thinBorderRange != (0, 100)

        self.width, self.height = self.baseTile.size
        self.transparant=Image.new("RGBA", (self.width, self.height))

        self.createBaseEdgeMasks()
        self.createThinBorder()
        self.createCorners()


    def createBaseEdgeMasks(self):
        margin = round(self.height / 100 * 10)
        mask = Image.new("RGBA", (self.width, self.height))
        pixels = list(self.transparant.getdata())
        mask.putdata(pixels)
        self.baseEdgeMask = mask.copy()

        pixels = [(0,0,0) if i < margin * self.height else p for i, p in enumerate(pixels)]
        for i in range(self.height):
             for p in range(self.width):
                if p < margin: pixels[i*self.height+p] = (0,0,0)
                if margin <= i < margin * 2:
                    if p < margin * 3 - i:
                        pixels[i*self.height+p] = (0,0,0)
        mask.putdata(pixels)
        self.baseCornerMask = mask
    
    def getBaseTile(self, position):
        tile = self.baseTile.copy()
        cornerPositions= BlobMap.getCornerPositions(position)
        for cornerPosition in cornerPositions:
            if cornerPosition is CornerPosition.topLeft:
                tile.paste(self.transparant, (0,0), self.baseCornerMask)
            if cornerPosition is CornerPosition.topRight: 
                tile.paste(self.transparant, (0,0), self.baseCornerMask.transpose(Image.FLIP_LEFT_RIGHT))
        return tile
        
    def generateBaseTileset(self):
        baseTileset = Image.new("RGBA", (self.width * 11, self.height * 5))
        for x in range(11):
            for y in range(5):
                pos = (x, y)
                if pos in BlobMap.skippedBaseTiles: continue
                base = self.getBaseTile(pos)
                posX = x * self.width
                posY = y * self.height
                loc = (posX, posY, posX + self.width, posY + self.height)
                baseTileset.paste(base, loc)
        return baseTileset


    def createThinBorder(self):
        pixels = list(self.borderTile.getdata())
        topHeight = round(self.height / 100 * self.thinBorderRange[0])
        bottomHeight = round(self.height / 100 * self.thinBorderRange[1])
        pixels = [p for i, p in enumerate(pixels) if topHeight * self.height < i < bottomHeight * self.height]
        self.thinBorderTile = Image.new("RGBA", (self.width, self.height))
        self.thinBorderTile.putdata(pixels)


    def createCorners(self):
        pixels = []
        for x in range(self.width):
            for y in range(self.height):
                if y <= self.height - x: pixels.append((0,0,0,0))
                else: pixels.append((0,0,0))
        mask = Image.new("RGBA", (self.width, self.height))
        mask.putdata(pixels)

        self.borderCorner = self.borderTile.copy()
        self.borderCorner.paste(self.transparant, (0,0), mask)

        self.thinBorderCorner = self.thinBorderTile.copy()
        self.thinBorderCorner.paste(self.transparant, (0,0), mask)

        self.borderDoubleCorner = self.borderCorner.copy()
        self.borderDoubleCorner.paste(self.transparant, (0,0), mask.transpose(Image.FLIP_LEFT_RIGHT))

        self.thinBorderDoubleCorner = self.thinBorderCorner.copy()
        self.thinBorderDoubleCorner.paste(self.transparant, (0,0), mask.transpose(Image.FLIP_LEFT_RIGHT))

        self.pointedLeftBorderCorner = self.borderTile.copy()
        self.pointedLeftBorderCorner.paste(self.transparant, (0,0), mask.transpose(Image.FLIP_TOP_BOTTOM))

        self.pointedRightBorderCorner = self.borderTile.copy()
        self.pointedRightBorderCorner.paste(self.transparant, (0,0), mask.transpose(Image.FLIP_TOP_BOTTOM)
            .transpose(Image.FLIP_LEFT_RIGHT))

    def generateBorderTileset(self):
        borderTileset = Image.new("RGBA", (self.width * 11, self.height * 5))

        for x in range(11):
            for y in range(5):
                pos = (x, y)
                
                orientations = BlobMap.getOrientations(pos)
                cornerPositions = BlobMap.getCornerPositions(pos)
                cornerPositions = cornerPositions if cornerPositions else [None]

                tiles = []
                for orientation in orientations:
                    for cornerPosition in cornerPositions:
                        cornerType = BlobMap.getCornerType(pos, orientation)
                        tiles.append(self.getBorder(orientation, cornerPosition, cornerType))
    
                posX = x * self.width
                posY = y * self.height
                loc = (posX, posY, posX + self.width, posY + self.height)
                [borderTileset.paste(tile, loc, tile) for tile in tiles]

        return borderTileset


    def getBorder(self, orientation, cornerPosition, cornerType):
        border = self.borderTile.copy() if orientation is Orientation.bottom else self.thinBorderTile.copy()
        if cornerType is CornerType.single:  
            border = self.borderCorner.copy() if orientation is Orientation.bottom else self.thinBorderCorner.copy()
        if cornerType is CornerType.double: 
            border = self.borderDoubleCorner.copy() if orientation is Orientation.bottom else self.thinBorderDoubleCorner.copy()

        if orientation is Orientation.left:
            border = border.rotate(90)
            if cornerPosition is CornerPosition.bottomLeft and cornerType is not CornerType.double:
                border = border.transpose(Image.FLIP_TOP_BOTTOM)

        if orientation is Orientation.right:
            border = border.rotate(-90)
            if cornerPosition is CornerPosition.topRight and cornerType is not cornerType.double:
                border = border.transpose(Image.FLIP_TOP_BOTTOM)

        if orientation is Orientation.top and cornerPosition is CornerPosition.topLeft:
            border = border.transpose(Image.FLIP_LEFT_RIGHT)

        if orientation is Orientation.bottom:
            if self.hasThinBorder:
                border = self.borderTile.copy()
            border = border.rotate(180)
            if cornerPosition is CornerPosition.bottomRight:
                border = border.transpose(Image.FLIP_LEFT_RIGHT)

        return border


def test():
    home = str(Path.home())
    bsim = home + "/test.png" #"./assets/base.png"
    brim = home + "/testBorder.png" #"./assets/border.png"
    baseImage = Image.open(bsim)
    borderImage = Image.open(brim)
    borderImage = borderImage.convert("RGBA")

    generator = Generator(baseImage, borderImage, (0, 10))
    baseTileset = generator.generateBaseTileset()
    borderTileset = generator.generateBorderTileset()

    baseTileset.paste(borderTileset, (0, 0), borderTileset)
    baseTileset.show()

test()