#!/usr/bin/sh

pykrita="$HOME/.local/share/krita/pykrita"

for desktopfile in *.desktop; do
    filename=${desktopfile##*/}
    scriptname=${filename%.*}

    cp -r "$filename" "$scriptname" "$pykrita"
done